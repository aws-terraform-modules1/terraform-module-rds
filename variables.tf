variable "db_name" {
  type        = string
  description = "(string, Required) name of the database"
  default     = "bubblegum"
}

variable "db_engine" {
  type        = string
  description = "(string, Required) postgres or i will come after you"
  default     = "postgres"
}

variable "engine_version" {
  type        = string
  description = "(string, Required) engine version"
  default     = "14.6-R1"
}

variable "db_instance_class" {
  type        = string
  description = "(string, Required) the type of ec2 instance that the db will run on"
  default     = "db.t2.micro"
}

variable "db_username" {
  type        = string
  description = "(string, Required) username for db"
  default     = "sa"
}

variable "db_password" {
  type        = string
  description = "(string, Required) password for db"
  default     = "123"
}
