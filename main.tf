resource "aws_db_instance" "silly_goose" {
  allocated_storage   = 8
  identifier          = var.db_name
  db_name             = var.db_name
  engine              = var.db_engine      #"postgres"
  engine_version      = var.engine_version #"14.6-R1"
  instance_class      = var.db_instance_class
  username            = var.db_username
  password            = var.db_password
  publicly_accessible = true
  skip_final_snapshot = true
}